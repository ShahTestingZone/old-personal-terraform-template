resource "aws_vpc" "terraform-vpc" {
  cidr_block = "20.0.0.0/16"

  tags = {
    Name = "terraform-vpc"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.terraform-vpc.id

  tags = {
    Name = "terraform-IGW"
  }
}

resource "aws_subnet" "private-subnet" {
  vpc_id     = aws_vpc.terraform-vpc.id
  cidr_block = "20.0.1.0/24"

  tags = {
    Name = "private-subnet-20.0.1.0/24"
  }
}

resource "aws_subnet" "public-subnet" {
  vpc_id     = aws_vpc.terraform-vpc.id
  cidr_block = "20.0.2.0/24"

  tags = {
    Name = "public-subnet-20.0.2.0/24"
  }
}


resource "aws_route_table" "public-rt" {
  vpc_id = aws_vpc.terraform-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "Public Route Table"
  }
}


resource "aws_default_route_table" "default" {
  default_route_table_id = aws_vpc.terraform-vpc.default_route_table_id

  route {
#    cidr_block = "0.0.0.0/0"
#    gateway_id = add natgateway id here
  }

  tags = {
    Name = "Private Route Table"
  }
}
resource "aws_route_table_association" "public-rta"{
  subnet_id      = aws_subnet.public-subnet.id
  route_table_id = aws_route_table.public-rt.id
}

resource "aws_route_table_association" "private-rta" {
  subnet_id      = aws_subnet.private-subnet.id
  route_table_id = aws_vpc.terraform-vpc.default_route_table_id
}

