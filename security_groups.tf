# Our default security group to access the instances over SSH and HTTP and HTTPS

resource "aws_security_group" "basic" {
  name        = "SSH_HTTP_HTTPS"
  description = "SSH_HTTP_HTTPS"
  vpc_id      = aws_vpc.terraform-vpc.id

  # SSH access from anywhere - make this secure by putting specific ip
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP and HTTPS access from the anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  tags = {
    Name = "Basic SSH_HTTP_HTTPS"
  }
}