resource "aws_instance" "web" {
  count ="1"
  ami = "ami-01f14919ba412de34"
  instance_type = "t2.micro"
  key_name = "Argos KeyPair"
  subnet_id = "subnet-011a44eb7ca8e3aaf"
  vpc_security_group_ids = ["sg-0793668157cf9a782",
                            "sg-0ac2ecaa9aeb18d29",
							"sg-02b46a2b84e4c1da2"
  ]

 tags = {
    Name = "Terraform Web Server"
  }
}
