terraform {
  backend "s3" {
    bucket = "terraform.shahtestingzone"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}

