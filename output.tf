output "vpc_id" {
  value = aws_vpc.terraform-vpc.id
}

output "Public_Subnet_ID" {
  value = aws_subnet.public-subnet.id
}

output "Private_Subnet_ID" {
  value = aws_subnet.private-subnet.id
}

output "Security_Group_ID" {
  value = aws_security_group.basic.id
}